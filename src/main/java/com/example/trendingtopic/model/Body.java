package com.example.trendingtopic.model;

import lombok.Data;

@Data
public class Body{

    private String id;
    private String url;
    private String text;
}