package com.example.trendingtopic.model;

import lombok.Data;

@Data
public class TopicResponse {

    String Url;
    String Text;
}
