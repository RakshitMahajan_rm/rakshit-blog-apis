package com.example.trendingtopic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class TrendingtopicApplication {

    public static void main(String[] args) {
        SpringApplication.run(TrendingtopicApplication.class, args);
    }

}
