package com.example.trendingtopic.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthController {
    public static final String OK = "ok";

    @RequestMapping("/healthcheck")
    @ResponseBody
    public String health() { return OK;}

}
