package com.example.trendingtopic.controller;

import com.example.trendingtopic.model.Body;
import com.example.trendingtopic.model.TopicResponse;
import com.example.trendingtopic.service.TrendingTopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class TrendingTopicController extends BaseController{

    @Autowired TrendingTopicService trendingTopicService;

    @GetMapping("/trending")
    ResponseEntity getTrendingTopics() {

        List<TopicResponse>  topicResponses;

        try{
              topicResponses = trendingTopicService.findTrendingTopics();

        } catch (Exception e) {

               return failure(null, HttpStatus.INTERNAL_SERVER_ERROR, e.toString());
        }

        return success(topicResponses, HttpStatus.OK, null);
    }

    @PostMapping("/trending")
    ResponseEntity postTopic( @RequestBody Body body) {

        String topicUrl = body.getUrl();
        String id = body.getId();
        String text = body.getText();

        topicUrl=topicUrl+"#"+id;

        Long topicId;

        try {

            topicId = trendingTopicService.saveTopic(topicUrl, text);

        } catch (Exception e) {

            return failure(null, HttpStatus.INTERNAL_SERVER_ERROR, e.toString());
        }

        return success(topicId, HttpStatus.OK, null);
    }


}


