package com.example.trendingtopic.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;

public class BaseController<T> {

    public ResponseEntity<T> success(T t, HttpStatus status, @Nullable String message) {
        return ResponseEntity.ok(t);
    }

    public ResponseEntity<T> failure(T t, HttpStatus status, @Nullable String message) {
        return ResponseEntity.status(status).body(t);
    }

}
