package com.example.trendingtopic.service;

import com.example.trendingtopic.model.Topic;
import com.example.trendingtopic.model.TopicResponse;
import com.example.trendingtopic.repository.TopicRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class TrendingTopicService {

    @Autowired private TopicRepository topicRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(TrendingTopicService.class);


    public List<TopicResponse> findTrendingTopics(){

        try{
                List<Topic> topics = topicRepository.findTop10ByOrderByCountDesc();

                List<TopicResponse> topicResponses = new ArrayList<>();

                topics.forEach(
                    topic -> {
                       TopicResponse topicResponse = new TopicResponse();
                           topicResponse.setUrl(topic.getTopicUrl());
                           topicResponse.setText(topic.getTitle());
                       if (topicResponse != null) topicResponses.add(topicResponse);
                });

                return topicResponses;

        }catch (Exception e) {
            LOGGER.error("error occured while retrieving Trending topics");
        return null;
        }
    }


    public Long saveTopic(String topicUrl, String text) {

        long millis = System.currentTimeMillis();
        Date date = new Date(millis);

        try {

            Topic topic = topicRepository.findByTitle(text);

            if (Objects.isNull(topic)) {
                Topic oldtopic = topicRepository.findByTopicUrl(topicUrl);

                if(!Objects.isNull(oldtopic)){topicRepository.delete(oldtopic);}

                Topic newtopic = new Topic();
                newtopic.setCount((long) 1);
                newtopic.setTopicUrl(topicUrl);
                newtopic.setDate(date);
                newtopic.setTitle(text);
                topicRepository.save(newtopic);

                return newtopic.getId();

            } else if (!topic.getTopicUrl().equals(topicUrl)) {

                topicRepository.delete(topic);

                Topic newtopic = new Topic();
                newtopic.setCount((long) 1);
                newtopic.setTopicUrl(topicUrl);
                newtopic.setDate(date);
                newtopic.setTitle(text);
                topicRepository.save(newtopic);

                return newtopic.getId();

            } else {

                if(!topic.getDate().toString().equals(date.toString())) {
                    topic.setCount((long) 0);
                    topic.setDate(date);
                }

                topic.setCount(topic.getCount()+1);
                topicRepository.save(topic);

                return topic.getId();
            }

        }catch (Exception e) {
            LOGGER.error("error occured while retrieving Trending topics");
            return null;
        }
    }

}
