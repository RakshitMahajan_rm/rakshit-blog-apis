package com.example.trendingtopic.repository;

import com.example.trendingtopic.model.Topic;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface TopicRepository extends JpaRepository<Topic , Long> {

    Topic findByTopicUrl(String topicUrl);

    Topic findByTitle(String text);

    List<Topic> findTop10ByOrderByCountDesc();

}
