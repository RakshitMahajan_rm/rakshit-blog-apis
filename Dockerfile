FROM openjdk:8-jdk-alpine
VOLUME /tmp
ARG APP_ENV
ENV NRA_VERSION 4.7.0
ENV NRA_LICENCE e08cb65a1311ea3b59b3c1f1e1a2f971035d3016
RUN mkdir -p /opt/newrelic/logs
RUN wget https://repo1.maven.org/maven2/com/newrelic/agent/java/newrelic-java/$NRA_VERSION/newrelic-java-$NRA_VERSION.zip -O /tmp/nr.zip && unzip /tmp/nr.zip -d /opt/ && rm /tmp/nr.zip
RUN chmod 777 /opt/newrelic/logs
RUN cat /opt/newrelic/newrelic.yml | sed -e "s/<%= license_key %>/$NRA_LICENCE/" -e "s/app_name:.*/app_name: blog-apis_$APP_ENV/" > /opt/newrelic/newrelic.yml.new
RUN mv /opt/newrelic/newrelic.yml /opt/newrelic/newrelic.yml.default
RUN mv /opt/newrelic/newrelic.yml.new /opt/newrelic/newrelic.yml
ENV spring.profiles.active=${APP_ENV}
ADD ./target/blog-apis-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-javaagent:/opt/newrelic/newrelic.jar", "-jar","/app.jar"]
